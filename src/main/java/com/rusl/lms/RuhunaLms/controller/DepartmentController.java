package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.model.DepartmentModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.DepartmentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-14
 * Version 1.0
 */

@RestController
@RequestMapping(path = "api/v1/lms")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;
    Logger logger = LogManager.getLogger("DepartmentController.class");

    @GetMapping(value = "/getDepartments")
    public ReturnFormat getDepartments(DepartmentModel departmentModel) {
        logger.info("/api/v1/lms/getDepartments");
        return departmentService.getDepartments(departmentModel);
    }

}
