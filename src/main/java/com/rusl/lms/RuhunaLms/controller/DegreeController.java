package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.model.DegreeModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.DegreeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */

@RestController
@RequestMapping(path = "/api/v1/lms")
public class DegreeController {
    @Autowired
    private DegreeService degreeService;
    Logger logger = LogManager.getLogger("DegreeController.class");

    @PostMapping(value = "/createDegree")
    public ReturnFormat createDegree(@RequestBody DegreeModel degreeModel){
        logger.info("/api/v1/lms/createDegree");
        return degreeService.createDegree(degreeModel);
    }
    @GetMapping("/getDegrees")
    public ReturnFormat getDegrees() {
        logger.info("/api/v1/lms/getDegrees");
        return degreeService.getDegrees();
    }
@GetMapping("/getDegreeTypes")
    public ReturnFormat getDegreeTypes(){
    logger.info("/api/v1/lms/getDegreeTypes");
    return degreeService.getDegreeTypes();
}
}
