package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.model.StudentModel;
import com.rusl.lms.RuhunaLms.model.UserModel;
import com.rusl.lms.RuhunaLms.model.UserStudentModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-11
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */

@RestController
@RequestMapping(path = "/api/v1/lms")
public class UserController {
    @Autowired
    private UserService userService;
    Logger logger = LogManager.getLogger("UserController.class");

    @PostMapping(value = "/createStudent")
    public ReturnFormat createStudent(@RequestBody UserStudentModel userStudentModel) {
        logger.info("/api/v1/lms/createStudent");
        return userService.createStudent(userStudentModel);
    }

    @PostMapping(value = "/createUser")
    public ReturnFormat createUser(@RequestBody UserModel userModel) {
        logger.info("/api/v1/lms/createUser");
        return userService.createUser(userModel);
    }

    @PutMapping(value = "/updateProfile")
    public ReturnFormat updateProfile(@RequestBody UserStudentModel userStudentModel) {
        logger.info("/api/v1/lms/updateProfile");
        return userService.updateProfile(userStudentModel);
    }

    @PostMapping("/createUserList")
    public List<UserModel> createUserList(@RequestBody List<UserModel> userModel) {
        return userService.createUserList(userModel);
    }

    @GetMapping("/getUsers")
    public List<UserModel> getUsers() {
        return userService.getUser();
    }

    @GetMapping("/getUserById")
    public ReturnFormat getUserById(@RequestParam int id) {
        logger.info("/api/v1/lms/getUserById");
        return userService.getUserById(id);
    }
}
