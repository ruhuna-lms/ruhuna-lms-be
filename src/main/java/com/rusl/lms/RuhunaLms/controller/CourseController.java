package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.dao.CourseModuleExamRepo;
import com.rusl.lms.RuhunaLms.model.*;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.CourseService;
import lombok.val;
import net.bytebuddy.asm.Advice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Updated by Rusiru on 2021-02-14
 * Version 1.0
 */

@RestController
@RequestMapping(path = "api/v1/lms")
public class CourseController {
    @Autowired
    private CourseService courseService;
    Logger logger = LogManager.getLogger("CourseController.class");

    @GetMapping(value = "/getCourses")
    public ReturnFormat getCourses() {
        logger.info("/api/v1/lms/getCourses");
        return courseService.getCourses();
    }

    @GetMapping(value = "/getCoursesByDegree")
    public ReturnFormat getAllCoursesByDegree(@RequestParam("degreeId") int degreeId) {
        logger.info("/api/v1/lms/getCoursesByDegree");
        return courseService.getAllCoursesByDegree(degreeId);
    }

    @GetMapping(value = "/getAlreadyConnectedCourses")
    public ReturnFormat getAlreadyConnectedCourses(@RequestParam("degreeId") int degreeId,
                                                   @RequestParam("semesterId") int semesterId) {
        logger.info("/api/v1/lms/getAlreadyConnectedCourses");
        return courseService.getAlreadyConnectedCourses(degreeId, semesterId);
    }

    @GetMapping(value = "/getAllEnrolledCourses")
    public ReturnFormat getAllEntrolledCourses(@RequestParam("sid") int sid) {
        logger.info("/api/v1/lms/getAllEnrolledCourses");
        return courseService.getAllEnrolledCourses(sid);
    }

    @PostMapping(value = "/addCourseForDegreeToSemester")
    public ReturnFormat addCourseForDegreeToSemester(@RequestBody CourseModuleExamModel courseModuleExamModel) {
        logger.info("/api/v1/lms/addCourseForDegreeToSemester");
        return courseService.addCourseForDegreeToSemester(courseModuleExamModel);
    }

    @PostMapping(value = "/addCourseToDegree")
    public ReturnFormat addCourseToDegree(@RequestBody CreditModel creditModel) {
        logger.info("/api/v1/lms/addCourseToDegree");
        return courseService.addCourseToDegree(creditModel);
    }

    @GetMapping(value = "/getCompulsoryStatus")
    public ReturnFormat getCompulsoryStatus() {
        logger.info("/api/v1/lms/getCompulsoryStatus");
        return courseService.getCompulsoryStatus();
    }

    @PostMapping(value = "/setEnrolledCourse")
    public ReturnFormat setEnrolledCourse(@RequestBody EnrolledCourseViewModel enrolledCourseViewModel) {
        logger.info("/api/v1/lms/setEnrolledCourse");
        return courseService.setEnrolledCourse(enrolledCourseViewModel);
    }

    @PostMapping(value = "/unEnrollCourse")
    public ReturnFormat unEnrollCourse(@RequestBody EnrolledCourseViewModel enrolledCourseViewModel) {
        logger.info("/api/v1/lms/unEnrollCourse");
        return courseService.unEnrollCourse(enrolledCourseViewModel);
    }

    @PostMapping(value = "/removeCourseFromDegree")
    public ReturnFormat removeCourseFromDegree(@RequestBody CreditModel creditModel) {
        logger.info("/api/v1/lms/removeCourseFromDegree");
        return courseService.removeCourseFromDegree(creditModel);
    }


    @PostMapping(value = "/removeCourseFromSemester")
    public ReturnFormat removeCourseFromSemester(@RequestBody CourseModuleExamModel courseModuleExamModel) {
        logger.info("/api/v1/lms/removeCourseFromSemester");
        return courseService.removeCourseFromSemester(courseModuleExamModel);
    }

    @GetMapping(value = "/getEnrolmentConfirmation")
    public ReturnFormat getEnrolmentConfirmation(@RequestParam("studentId") int studentId,
                                                 @RequestParam("semesterId") int semesterId) {

        logger.info("/api/v1/lms/getEnrolmentConfirmation");
        return courseService.getEnrolmentConfirmation(studentId, semesterId);
    }


    @PostMapping(value = "/confirmEnrolment")
    public ReturnFormat confirmEnrolment(@RequestBody ConfirmEnrollmentModel confirmEnrollmentModel) {
        logger.info("/api/v1/lms/confirmEnrolment");
        return courseService.confirmEnrolment(confirmEnrollmentModel);
    }

    @PostMapping(value = "/removeEnrolmentConfirmation")
    public ReturnFormat removeEnrolmentConfirmation(@RequestBody RemoveEnrolmentConfirmationModel removeEnrolmentConfirmationModel) {
        logger.info("/api/v1/lms/removeEnrolmentConfirmation");
        return courseService.removeEnrolmentConfirmation(removeEnrolmentConfirmationModel);
    }
}