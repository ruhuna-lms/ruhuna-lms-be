package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.model.SemesterModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.SemesterService;
import com.rusl.lms.RuhunaLms.service.StudentSemesterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-14
 * Version 1.0
 */
@RestController
@RequestMapping(path = "api/v1/lms")
public class SemesterController {
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private StudentSemesterService studentSemesterService;
    Logger logger = LogManager.getLogger("SemesterController.class");

    @GetMapping("/getSemesters")
    public ReturnFormat getSemesters() {
        logger.info("/api/v1/lms/getSemesters");
        return semesterService.getSemesters();
    }

    @GetMapping("/getCurrentSemesterForDegree")
    public ReturnFormat getCurrentSemesterForDegree(@RequestParam ("degreeId") int degreeId) {
        logger.info("/api/v1/lms/getCurrentSemesterForDegree");
        return semesterService.getCurrentSemesterForDegree(degreeId);
    }

    @GetMapping("/getSemesterForStudent")
    public ReturnFormat getSemesterForStudent(@RequestParam ("studentId")int studentId) {
        logger.info("/api/v1/lms/getSemesterForStudent");
        return studentSemesterService.getSemesterForStudent(studentId);
    }

    @PostMapping("/createSemester")
    public ReturnFormat createSemester(@RequestBody SemesterModel semesterModel){
        logger.info("/api/v1/lms/createSemester");
        return semesterService.createSemester(semesterModel);
    }
}
