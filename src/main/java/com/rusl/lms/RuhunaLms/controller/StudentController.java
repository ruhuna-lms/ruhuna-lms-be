package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.model.StudentModel;
import com.rusl.lms.RuhunaLms.model.UserStudentModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.StudentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Updated by Rusiru on 2022-02-20
 * Version 1.0
 */
@RestController
@RequestMapping(path = "/api/v1/lms")
public class StudentController {
    @Autowired
    private StudentService studentService;
    Logger logger = LogManager.getLogger("StudentController.class");


    @GetMapping(value = "/getStudents")
    public ReturnFormat getStudents(){
        logger.info("/api/v1/lms/getStudents");
        return studentService.getStudents();
    }
    @GetMapping(value = "/getStudentDetail")
    public ReturnFormat getStudentDetail(@RequestParam int studentId){
        logger.info("/api/v1/lms/getStudentDetail");
        return studentService.getStudentDetail(studentId);
    }

    @GetMapping(value = "/getStudentsForCourse")
    public ReturnFormat getStudentsForCourse(@RequestParam int degreeId,
                                             @RequestParam int semesterId,
                                             @RequestParam int courseId){
        logger.info("/api/v1/lms/getStudentsForCourse");
        return studentService.getStudentsForCourse(degreeId,semesterId,courseId);
    }

    @PostMapping(value = "/studentUpload")
    public ReturnFormat studentUpload(@RequestBody UserStudentModel[] userStudentModel){
        logger.info("/api/v1/lms/studentUpload");
        return studentService.studentUpload(userStudentModel);
    }
}
