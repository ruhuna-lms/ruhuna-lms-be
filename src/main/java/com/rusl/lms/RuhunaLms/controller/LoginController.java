package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.config.PasswordEncoder;
import com.rusl.lms.RuhunaLms.model.*;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.LoginService;
import com.rusl.lms.RuhunaLms.service.StudentService;
import com.rusl.lms.RuhunaLms.utils.JwtUtil;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/lms")
public class LoginController {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private LoginService loginService;
    @Autowired
    private StudentService studentService;


    Logger logger = LogManager.getLogger("LoginController.class");

    @GetMapping("/userProfile")
    public String welcome() {
        return "Hello";
    }

    @PostMapping("/authenticate")
    public ReturnFormat generateToken(@RequestBody LoginModel authRequest) throws Exception {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            PasswordEncoder passwordEncoder = new PasswordEncoder();
            String s = passwordEncoder.encodePassword(authRequest.getPassword());
            System.out.println(s);
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), s));
        } catch (Exception e) {
            returnFormat.setMessage(Messages.INVALID_LOGIN_CREDENTIALS);
            return returnFormat;

        }
        LoginModel result;
        JwtModel jwtModel = new JwtModel();
        StudentModel studentModel = new StudentModel();
        String jwtToken = jwtUtil.generateToken(authRequest.getUsername());
        result = loginService.getAuthorizedUser(authRequest.getUsername());
        jwtModel.setJwtToken(jwtToken);
        jwtModel.setId(result.getId());
        jwtModel.setUsername(result.getUsername());
        jwtModel.setUserrole_id(result.getUserrole_id());
        if (result.getUserrole_id() == 2) {
            studentModel = studentService.getStudentByUserId(result.getId());
            jwtModel.setStudent_id(studentModel.getId());
            jwtModel.setDegree_id(studentModel.getDegree_id());
        }


        returnFormat.setData(jwtModel);
        logger.info("/api/v1/lms/authenticate");
        return returnFormat;
    }

    @PutMapping (value = "/passwordRest")
    public ReturnFormat passwordRest(@RequestBody PasswordResetModel passwordResetModel) {
        logger.info("/api/v1/lms/passwordRest");
        return loginService.passwordRest(passwordResetModel);
    }
}
