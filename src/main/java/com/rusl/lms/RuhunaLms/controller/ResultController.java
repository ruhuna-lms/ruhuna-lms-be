package com.rusl.lms.RuhunaLms.controller;

import com.rusl.lms.RuhunaLms.helper.ExcelHelper;
import com.rusl.lms.RuhunaLms.helper.ResponseMessage;
import com.rusl.lms.RuhunaLms.model.UploadResultModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.service.ResultService;
import lombok.val;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-21
 * Updated by Rusiru on 2022-02-20
 * Version 1.0
 */
@RestController
@RequestMapping(path = "api/v1/lms")
public class ResultController {
    @Autowired
    private ResultService resultService;
    Logger logger = LogManager.getLogger("ResultController.class");

    @GetMapping(value = "/getStudentResults")
    public ReturnFormat getStudentResults(@RequestParam int studentId,
                                          @RequestParam int semesterId) {
        logger.info("/api/v1/lms/getStudentResults");
        return resultService.getStudentResults(studentId, semesterId);

    }
    @GetMapping(value = "/getStudentResultsAllSemesters")
    public ReturnFormat getStudentResults(@RequestParam int studentId) {
        logger.info("/api/v1/lms/getStudentResultsAllSemesters");
        return resultService.getStudentResultsAllSemesters(studentId);

    }

    @PostMapping(value = "/resultsUpload")
    public ReturnFormat resultsUpload(@RequestBody UploadResultModel resultModel) {
        logger.info("/api/v1/lms/resultsUpload");
        return resultService.resultsUpload(resultModel);
    }
    @GetMapping(value = "/getCurrentGpa")
    public ReturnFormat getCurrentGpa(@RequestParam int studentId){
        logger.info("/api/v1/lms/getCurrentGpa");
        return resultService.getCurrentGpa(studentId);
    }
}
