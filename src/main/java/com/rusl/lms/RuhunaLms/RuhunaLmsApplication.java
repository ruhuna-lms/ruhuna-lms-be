package com.rusl.lms.RuhunaLms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-11
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */
public class RuhunaLmsApplication extends SpringBootServletInitializer{
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(RuhunaLmsApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(RuhunaLmsApplication.class, args);
		Logger logger = LogManager.getLogger(RuhunaLmsApplication.class);
		logger.info("Application Start Running...");

	}


}
