package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.dao.SemesterRepo;
import com.rusl.lms.RuhunaLms.dao.StudentsSemestersRepo;
import com.rusl.lms.RuhunaLms.model.SemesterViewModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-21
 * Version 1.0
 */
@Service
public class StudentSemesterService {
    @Autowired
    private StudentsSemestersRepo studentsSemestersRepo;
    Logger logger = LogManager.getLogger("StudentSemesterService.class");

    public ReturnFormat getSemesterForStudent(int studentId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<SemesterViewModel> semesterViewModelList = studentsSemestersRepo.getSemesterForStudent(studentId);
            if (semesterViewModelList.size() != 0) {
                returnFormat.setData(semesterViewModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }
}
