package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.config.PasswordEncoder;
import com.rusl.lms.RuhunaLms.dao.*;
import com.rusl.lms.RuhunaLms.helper.UploadStudentHelper;
import com.rusl.lms.RuhunaLms.model.*;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Updated by Rusiru on 2022-02-20
 * Version 1.0
 */
@Service
public class StudentService {
    @Autowired
    private CourseModuleExamRepo courseModuleExamRepo;
    @Autowired
    private StudentViewRepo studentViewRepo;
    @Autowired
    private StudentRepo studentRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ResultRepo resultRepo;
    @Autowired
    private StudentSaveRepo studentSaveRepo;
    @Autowired
    private UserCredentialRepo userCredentialRepo;
    @Autowired
    private RelatedStudentsInformationRepo relatedStudentsInformationRepo;

    final static int STUDENT = 2;
    Logger logger = LogManager.getLogger("StudentService.class");


    public ReturnFormat getStudents() {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<StudentViewModel> studentModelList = studentViewRepo.getStudents();
            if (studentModelList.size() != 0) {
                returnFormat.setData(studentModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getStudentDetail(int studentId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            returnFormat.setData(studentViewRepo.findById(studentId));
            returnFormat.setMessage(Messages.Success);
            returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public StudentModel getStudentByUserId(int userId) {
        StudentModel studentModel = new StudentModel();
        try {
            studentModel = studentRepo.getStudentByUserId(userId);

        } catch (JDBCConnectionException e) {
            logger.error(e);

        } catch (Exception e) {
            logger.error(e);
        }
        return studentModel;
    }

    public ReturnFormat studentUpload(UserStudentModel[] userStudentModel) {
        ReturnFormat returnFormat = new ReturnFormat();

        try {

            List<UploadStudentHelper> notSavedStudentsList = new ArrayList<>();
            if (!userStudentModel.equals(null)) {


                try {

                    for (int i = 0; i < userStudentModel.length; i++) {
                        UploadStudentHelper uploadStudentHelper = new UploadStudentHelper();
                        UserStudentModel studentDetails = userStudentModel[i];
                        UserModel userModel = new UserModel();
                        userModel.setUsername(studentDetails.getUsername());
                        userModel.setTitle(studentDetails.getTitle());
                        userModel.setFirstname(studentDetails.getFirstname());
                        userModel.setLastname(studentDetails.getLastname());
                        userModel.setAddress(studentDetails.getAddress());
                        userModel.setDob(studentDetails.getDob());
                        userModel.setDepartment_id(studentDetails.getDepartment_id());
                        if (studentDetails.getGender().equals("F") || studentDetails.getGender().equals("M")) {
                            if (studentDetails.getUserrole_id() == STUDENT) {
                                if (studentDetails.getContact_number().length() <= 10) {
                                    userModel.setContact_number(studentDetails.getContact_number());
                                    userModel.setGender(studentDetails.getGender());
                                    userModel.setUserrole_id(studentDetails.getUserrole_id());
                                    try {
                                        userRepo.save(userModel);
                                        if (userModel.getUserrole_id() == STUDENT) {
                                            int getUserId = userModel.getId();
                                            StudentModel studentModel = new StudentModel();
                                            studentModel.setUser_id(getUserId);
                                            studentModel.setResgistration_number(studentDetails.getResgistration_number());
                                            studentModel.setDegree_id(studentDetails.getDegree_id());
                                            studentModel.setIndex_number(studentDetails.getIndex_number());
                                            studentModel.setLevel(studentDetails.getLevel());
                                            studentModel.setName_with_initials(studentDetails.getName_with_initials());
                                            studentModel.setFull_name(studentDetails.getFull_name());
                                            studentModel.setSpecialization_id(studentDetails.getSpecialization_id());
                                            try {
                                                studentSaveRepo.save(studentModel);
                                                UserCredentialModel userCredentialModel = new UserCredentialModel();
                                                userCredentialModel.setUser_id(userModel.getId());
                                                PasswordEncoder passwordEncoder = new PasswordEncoder();
                                                userCredentialModel.setPassword(passwordEncoder.encodePassword(studentDetails.getUsername()));
                                                userCredentialRepo.save(userCredentialModel);
                                            } catch (Exception ex) {
                                                String getErrorMsg = ex.getCause().getCause().getMessage();
                                                if(getErrorMsg.contains("fk_student_degree1")){
                                                    userRepo.deleteById(getUserId);
                                                    uploadStudentHelper.setRegistrationNumber(studentDetails.getResgistration_number());
                                                    uploadStudentHelper.setErrorMessage("Degree does not exits");
                                                    notSavedStudentsList.add(uploadStudentHelper);
                                                }else if(getErrorMsg.contains("fk_specialization_id")){
                                                    userRepo.deleteById(getUserId);
                                                    uploadStudentHelper.setRegistrationNumber(studentDetails.getResgistration_number());
                                                    uploadStudentHelper.setErrorMessage("Specialization does not exits");
                                                    notSavedStudentsList.add(uploadStudentHelper);
                                                }


                                            }


                                        }
                                    } catch (Exception e) {
                                        String userExists = e.getMessage();
                                        if (userExists.contains("username_UNIQUE")) {
                                            uploadStudentHelper.setRegistrationNumber(studentDetails.getResgistration_number());
                                            uploadStudentHelper.setErrorMessage("Username already exits");
                                            notSavedStudentsList.add(uploadStudentHelper);

                                        }
                                    }


                                } else {
                                    uploadStudentHelper.setRegistrationNumber(studentDetails.getResgistration_number());
                                    uploadStudentHelper.setErrorMessage("Incorrect Contact Number");
                                    notSavedStudentsList.add(uploadStudentHelper);
//
                                }

                            } else {
                                uploadStudentHelper.setRegistrationNumber(studentDetails.getResgistration_number());
                                uploadStudentHelper.setErrorMessage("Not a Student");
                                notSavedStudentsList.add(uploadStudentHelper);
//
                            }
                        } else {
                            uploadStudentHelper.setRegistrationNumber(studentDetails.getResgistration_number());
                            uploadStudentHelper.setErrorMessage("Incorrect Gender");
                            notSavedStudentsList.add(uploadStudentHelper);

                        }


                    }
                    if (notSavedStudentsList.size() == 0) {

                        returnFormat.setMessage("All students were saved");
                        returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                    } else {
                        returnFormat.setData(notSavedStudentsList);
                        returnFormat.setMessage("Not All Students Saved");
                        returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }

        return returnFormat;
    }

    public ReturnFormat getStudentsForCourse(int degreeId, int semesterId, int courseId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            RelatedStudentInformationModel relatedStudentInformationModel = new RelatedStudentInformationModel();
            CourseModuleExamModel getCourseModuleId = new CourseModuleExamModel();
            getCourseModuleId = courseModuleExamRepo.getCourseModuleExamId(degreeId, semesterId, courseId);
            if (getCourseModuleId != null) {

                List<RelatedStudentInformationModel>  getRelatedStudentsInformation = relatedStudentsInformationRepo.getRelatedStudentsList(getCourseModuleId);
                if(getRelatedStudentsInformation !=null){
                    returnFormat.setData(getRelatedStudentsInformation);
                    returnFormat.setMessage("Related Students Information");
                    returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                }else {
                    returnFormat.setData(null);
                    returnFormat.setMessage("Related Students Are Not Found");
                    returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                    return returnFormat;
                }

            }else {
                returnFormat.setData(null);
                returnFormat.setMessage("Course Module Exam Id Not Found");
                returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                return returnFormat;
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

}
