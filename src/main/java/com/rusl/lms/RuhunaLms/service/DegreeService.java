package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.dao.DegreeRepo;
import com.rusl.lms.RuhunaLms.dao.DegreeTypeRepo;
import com.rusl.lms.RuhunaLms.model.DegreeModel;
import com.rusl.lms.RuhunaLms.model.DegreeTypeModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */

@Service
public class DegreeService {
    @Autowired
    private DegreeRepo degreeRepo;
    @Autowired
    private DegreeTypeRepo degreeTypeRepo;

    Logger logger = LogManager.getLogger("DegreeService.class");

    public ReturnFormat createDegree(DegreeModel degreeModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            if (!degreeModel.equals(null)) {

                returnFormat.setData(degreeRepo.save(degreeModel));
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);

            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.DatabaseError);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getDegrees() {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<DegreeModel> degreeModelList = degreeRepo.findAll();
            if (degreeModelList != null) {
                returnFormat.setData(degreeModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }

        return returnFormat;
    }

    public ReturnFormat getDegreeTypes() {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
        List<DegreeTypeModel> degreeTypeModelList = degreeTypeRepo.findAll();
        if(degreeTypeModelList.size()!=0){
            returnFormat.setData(degreeTypeModelList);
            returnFormat.setMessage(Messages.Success);
            returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
        } else {
            returnFormat.setMessage(Messages.NoRecords);
            returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
        }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }
}
