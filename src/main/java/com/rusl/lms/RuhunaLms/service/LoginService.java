package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.config.PasswordEncoder;
import com.rusl.lms.RuhunaLms.dao.LoginRepo;
import com.rusl.lms.RuhunaLms.dao.UserCredentialRepo;
import com.rusl.lms.RuhunaLms.dao.UserRepo;
import com.rusl.lms.RuhunaLms.filter.JwtFilter;
import com.rusl.lms.RuhunaLms.model.LoginModel;
import com.rusl.lms.RuhunaLms.model.PasswordResetModel;
import com.rusl.lms.RuhunaLms.model.UserCredentialModel;
import com.rusl.lms.RuhunaLms.model.UserModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private LoginRepo loginRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    UserCredentialRepo userCredentialRepo;
    Logger logger = LogManager.getLogger("LoginService.class");

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginModel loginModel = loginRepo.findByusername(username);
        return new User(loginModel.getUsername(), loginModel.getPassword(), new ArrayList<>());

    }

    public LoginModel getAuthorizedUser(String username) {
        LoginModel loginModel = loginRepo.findByusername(username);
        return loginModel;
    }

    public ReturnFormat passwordRest(PasswordResetModel passwordResetModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        PasswordResetModel resetModel = new PasswordResetModel();
        Optional<UserCredentialModel> userCredentialModel = Optional.of(new UserCredentialModel());
        Optional<UserModel> userModel = Optional.of(new UserModel());
        try {
            if (!passwordResetModel.equals(null)) {

                try {
                    userModel = Optional.ofNullable(userRepo.findById(passwordResetModel.getId()).orElseThrow(() -> new IllegalArgumentException("not found")));

                    String oldPassword = passwordResetModel.getOldPassword();
                    String newPassword = passwordResetModel.getNewPassword();
                    PasswordEncoder passwordEncoder = new PasswordEncoder();
                    String encryptedNewPassword = passwordEncoder.encodePassword(newPassword);
                    String encryptedOldPassword = passwordEncoder.encodePassword(oldPassword);

                    if (oldPassword.equals(newPassword)) {
                        returnFormat.setMessage("new password cannot be the same as old");
                    } else if (passwordResetModel.getNewPassword().length() <= 4) {
                        returnFormat.setMessage("your password does not satisfy current policy requirements");
                    } else {

                        userCredentialModel = userCredentialRepo.getOldPassword(passwordResetModel.getId());
                        if(!userCredentialModel.get().getPassword().equals(encryptedOldPassword)){
                            returnFormat.setData(null);
                            returnFormat.setMessage("The password you entered is incorrect Please retype your current password");
                            returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                        }else if (!userCredentialModel.get().getPassword().equals(encryptedNewPassword)) {
                            UserCredentialModel saveNewCredentials = new UserCredentialModel();
                            saveNewCredentials.setPassword(encryptedNewPassword);
                            returnFormat.setData(userCredentialRepo.updatePassword(userCredentialModel.get().getUser_id(), encryptedNewPassword));
                            returnFormat.setMessage(Messages.SuccessUpdate);
                            returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                        } else {
                            returnFormat.setData(null);
                            returnFormat.setMessage("password mismatch");
                            returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                        }
                    }
                } catch (IllegalArgumentException illegalArgumentException) {
                    returnFormat.setData(null);
                    returnFormat.setMessage("User Not Found");
                    returnFormat.setStatus(ReturnFormat.Status.ERROR);
                }


            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }
}
