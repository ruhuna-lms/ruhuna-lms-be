package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.dao.CurrentSemesterRepo;
import com.rusl.lms.RuhunaLms.dao.SemesterRepo;
import com.rusl.lms.RuhunaLms.model.CurrentSemesterModel;
import com.rusl.lms.RuhunaLms.model.SemesterModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Version 1.0
 */
@Service
public class SemesterService {
    @Autowired
    private SemesterRepo semesterRepo;
    @Autowired
    private CurrentSemesterRepo currentSemesterRepo;
    Logger logger = LogManager.getLogger("SemesterRepo.class");

    public ReturnFormat getSemesters() {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<SemesterModel> semesterModelList = semesterRepo.findAll();
            if (semesterModelList.size() != 0) {
                returnFormat.setData(semesterModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat createSemester(SemesterModel semesterModel) {
        ReturnFormat returnFormat = new ReturnFormat();

        try {
            if (!semesterModel.equals(null)) {
                returnFormat.setData(semesterRepo.save(semesterModel));
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }


        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getCurrentSemesterForDegree(int degreeId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<CurrentSemesterModel> currentSemesterModelList = currentSemesterRepo.getSemester(degreeId);
            if (currentSemesterModelList.size() != 0) {
                returnFormat.setData(currentSemesterModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

}
