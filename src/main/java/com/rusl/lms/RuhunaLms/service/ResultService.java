package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.dao.*;
import com.rusl.lms.RuhunaLms.helper.ResultHelper;
import com.rusl.lms.RuhunaLms.helper.UploadExamResultReturnHelper;
import com.rusl.lms.RuhunaLms.model.*;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-21
 * Updated by Rusiru on 2022-02-20
 * Version 1.0
 */

@Service
public class ResultService {
    @Autowired
    private ResultViewRepo resultViewRepo;
    @Autowired
    private UploadResults uploadResults;
    @Autowired
    private CourseModuleExamRepo courseModuleExamRepo;
    @Autowired
    private StudentRepo studentRepo;
    @Autowired
    private ResultRepo resultRepo;
    Logger logger = LogManager.getLogger("ResultService.class");
    @Autowired
    private GpaCalculatorRepo gpaCalculatorRepo;

    public ReturnFormat getStudentResults(int studentId, int semesterId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<ResultViewModel> resultViewModelList = resultViewRepo.getResults(studentId, semesterId);
            if (resultViewModelList.size() != 0) {
                returnFormat.setData(resultViewModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat resultsUpload(UploadResultModel detailModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<String> notEnrolledStudents = new ArrayList<>();
            List<String> notRegisteredStudents = new ArrayList<>();

            if (detailModel != null) {

                CourseModuleExamModel getCourseModuleId = new CourseModuleExamModel();
                getCourseModuleId = courseModuleExamRepo.getCourseModuleExamId(detailModel.getDegreeId(), detailModel.getSemesterId(), detailModel.getCourseId());

                if (getCourseModuleId != null) {

                    ExcelModel[] uploadResultsModels = detailModel.getFile();
                    ResultHelper resultHelper = new ResultHelper();
                    Map resultMap = resultHelper.getHashtable();
                    for (int i = 0; i < uploadResultsModels.length; i++) {
                        ExcelModel excelModel = uploadResultsModels[i];
                        StudentModel studentModel = studentRepo.getStudentByRegistrationNumber(excelModel.getIndexNumber());
                        if (studentModel != null) {
                            Integer resultTypeId = (Integer) resultMap.get(excelModel.getResult());
                            ResultModel resultModel = resultRepo.getResult(studentModel.getId(), getCourseModuleId.getId());
                            if (resultModel != null) {
                                resultRepo.updateResult(resultTypeId, resultModel.getId());
                            } else {
                                notEnrolledStudents.add(excelModel.getIndexNumber());
                            }
                        } else {
                            notRegisteredStudents.add(excelModel.getIndexNumber());
                        }
                    }

                } else {
                    returnFormat.setData(null);
                    returnFormat.setMessage("Course Module Exam Id Not Found");
                    returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                    return returnFormat;
                }

            }
            UploadExamResultReturnHelper uploadExamResultReturnHelper = new UploadExamResultReturnHelper();
            if (notRegisteredStudents.size() != 0) {
                uploadExamResultReturnHelper.setNotRegisteredStudentList(notRegisteredStudents);
            }
            if (notEnrolledStudents.size() != 0) {
                uploadExamResultReturnHelper.setNotEnrolledStudentList(notEnrolledStudents);
            }
            if (notEnrolledStudents.size() == 0 && notRegisteredStudents.size() == 0) {
                returnFormat.setMessage("All Results Saved");
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(uploadExamResultReturnHelper);
                returnFormat.setMessage("Not All Results Saved");
                returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnFormat;
    }

    public ReturnFormat getCurrentGpa(int studentId) {
        ReturnFormat returnFormat = new ReturnFormat();
        Double totalMarks = 0.00;
        Double totalCredits = 0.00;
        Double calculatedGpa = 0.00;
        try {
            List<GpaModel> gpaModelList = gpaCalculatorRepo.getMarks(studentId);

            if (gpaModelList != null) {
                for (GpaModel gpaModel : gpaModelList) {
                    if (gpaModel.getMark() != -1) {
                        totalMarks = totalMarks + (gpaModel.getCredits() * gpaModel.getMark());
                        totalCredits = totalCredits + (gpaModel.getCredits());
                    } else {

                    }
                }
                calculatedGpa = totalMarks / totalCredits;
                returnFormat.setData(calculatedGpa);
                returnFormat.setMessage("GPA Calculated");
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage("Student has not enrolled for any gpa calculated courses");
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getStudentResultsAllSemesters(int studentId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<ResultViewModel> resultViewModelList = resultViewRepo.getAllSemesterResults(studentId);
            if (resultViewModelList.size() != 0) {
            returnFormat.setData(resultViewModelList);
            returnFormat.setMessage(Messages.Success);
            returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
        } else {
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.NoRecords);
        }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

}

