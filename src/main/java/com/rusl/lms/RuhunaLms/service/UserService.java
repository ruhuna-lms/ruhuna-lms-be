package com.rusl.lms.RuhunaLms.service;


import com.rusl.lms.RuhunaLms.config.PasswordEncoder;
import com.rusl.lms.RuhunaLms.dao.StudentSaveRepo;
import com.rusl.lms.RuhunaLms.dao.UserCredentialRepo;
import com.rusl.lms.RuhunaLms.dao.UserRepo;
import com.rusl.lms.RuhunaLms.model.StudentModel;
import com.rusl.lms.RuhunaLms.model.UserCredentialModel;
import com.rusl.lms.RuhunaLms.model.UserModel;
import com.rusl.lms.RuhunaLms.model.UserStudentModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-11
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private StudentSaveRepo studentSaveRepo;
    @Autowired
    private UserCredentialRepo userCredentialRepo;


    Logger logger = LogManager.getLogger("UserService.class");
    final static int STUDENT = 2;

    public ReturnFormat createUser(UserModel userModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        if (!userModel.equals(null)) {
            try {

            } catch (Exception e) {

            }
        }
        return returnFormat;
    }

    public ReturnFormat createStudent(UserStudentModel userStudentModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        UserModel userModel = new UserModel();
        try {
            if (!userStudentModel.equals(null)) {

                try {
                    userModel.setUsername(userStudentModel.getUsername());
                    userModel.setTitle(userStudentModel.getTitle());
                    userModel.setFirstname(userStudentModel.getFirstname());
                    userModel.setLastname(userStudentModel.getLastname());
                    userModel.setAddress(userStudentModel.getAddress());
                    userModel.setGender(userStudentModel.getGender());
                    userModel.setDob(userStudentModel.getDob());
                    userModel.setContact_number(userStudentModel.getContact_number());
                    userModel.setUserrole_id(userStudentModel.getUserrole_id());
                    userModel.setDepartment_id(userStudentModel.getDepartment_id());

                    returnFormat.setData(userRepo.save(userModel));
                    returnFormat.setMessage(Messages.Success);
                    returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                    if (userModel.getUserrole_id() == STUDENT) {
                        int getUserId = userModel.getId();
                        StudentModel studentModel = new StudentModel();
                        studentModel.setUser_id(getUserId);
                        studentModel.setResgistration_number(userStudentModel.getResgistration_number());
                        studentModel.setDegree_id(userStudentModel.getDegree_id());
                        studentModel.setIndex_number(userStudentModel.getIndex_number());
                        studentModel.setLevel(userStudentModel.getLevel());
                        studentModel.setName_with_initials(userStudentModel.getName_with_initials());
                        studentModel.setFull_name(userStudentModel.getFull_name());
                        studentModel.setSpecialization_id(userStudentModel.getSpecialization_id());

                        returnFormat.setData(studentSaveRepo.save(studentModel));
                    }
                    UserCredentialModel userCredentialModel = new UserCredentialModel();
                    userCredentialModel.setUser_id(userModel.getId());
                    PasswordEncoder passwordEncoder = new PasswordEncoder();
                    userCredentialModel.setPassword(passwordEncoder.encodePassword(userModel.getUsername()));
                    userCredentialRepo.save(userCredentialModel);

                } catch (Exception e) {
                    String detailMessage = e.getMessage();
                    if (detailMessage.contains("username_UNIQUE")) {
                        returnFormat.setMessage("Username already exits");
                        return returnFormat;
                    }
                }


            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }

        return returnFormat;
    }

    public List<UserModel> createUserList(List<UserModel> userModel) {
        return userRepo.saveAll(userModel);
    }

    public List<UserModel> getUser() {
        return userRepo.findAll();
    }

    public ReturnFormat getUserById(int id) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            returnFormat.setData(userRepo.findById(id));
            returnFormat.setMessage(Messages.Success);
            returnFormat.setStatus(ReturnFormat.Status.SUCCESS);


        } catch (
                JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (
                Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }


    public ReturnFormat updateProfile(UserStudentModel userStudentModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        StudentModel studentModel = new StudentModel();
        try {
            if (!userStudentModel.equals(null)) {

                userRepo.updateProfile(userStudentModel.getId(), userStudentModel.getTitle(), userStudentModel.getFirstname(), userStudentModel.getLastname(), userStudentModel.getAddress(), userStudentModel.getGender(), userStudentModel.getDob(), userStudentModel.getContact_number());
                studentSaveRepo.updateStudent(userStudentModel.getId(), userStudentModel.getResgistration_number(), userStudentModel.getDegree_id(), userStudentModel.getIndex_number(), userStudentModel.getLevel(), userStudentModel.getName_with_initials(), userStudentModel.getFull_name(), userStudentModel.getSpecialization_id());
                returnFormat.setMessage(Messages.SuccessUpdate);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.InternalServerError);
                returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }


}


