package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.dao.*;
import com.rusl.lms.RuhunaLms.model.*;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Version 1.0
 */

@Service
public class CourseService {
    @Autowired
    private CourseRepo courseRepo;
    @Autowired
    private AlreadyConnectedCourseRepo alreadyConnectedCourseRepo;
    @Autowired
    private EnrollCoursesRepo enrollCoursesRepo;
    @Autowired
    private CreditViewRepo creditViewRepo;
    @Autowired
    private CreditRepo creditRepo;
    @Autowired
    private CourseModuleExamRepo courseModuleExamRepo;
    @Autowired
    private CompulsoryRepo compulsoryRepo;
    @Autowired
    private EnrolledCourseViewRepo enrolledCourseViewRepo;
    @Autowired
    private ResultRepo resultRepo;
    @Autowired
    ConfirmEnrollmentRepo confirmEnrollmentRepo;
    @Autowired
    RemoveEnrolmentConfirmationRepo removeEnrolmentConfirmationRepo;
    Logger logger = LogManager.getLogger("CourseService.class");

    public ReturnFormat getCourses() {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<CourseModel> courseModelList = courseRepo.findAll();
            if (courseModelList != null) {
                returnFormat.setData(courseModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getAllCoursesByDegree(int degreeId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<CreditViewModel> courseModelList = creditViewRepo.getAllCoursesByDegree(degreeId);

            if (courseModelList.size() != 0) {
                returnFormat.setData(courseModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setMessage(Messages.NoRecords);
                returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.DatabaseError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }


    public ReturnFormat getAlreadyConnectedCourses(int degreeId, int semesterId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<AlreadyConnectedCourseModel> alreadyConnectedCourseModelList = alreadyConnectedCourseRepo.getAlreadyConnectedCourses(degreeId, semesterId);
            if (alreadyConnectedCourseModelList.size() != 0) {
                returnFormat.setData(alreadyConnectedCourseModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setMessage(Messages.NoRecords);
                returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getAllEnrolledCourses(int sid) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<EnrolledCoursesModel> enrolledCoursesModelList = enrollCoursesRepo.getAllEnrollCourses(sid);
            if (enrolledCoursesModelList.size() != 0) {
                returnFormat.setData(enrolledCoursesModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setMessage(Messages.NoRecords);
                returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat addCourseForDegreeToSemester(CourseModuleExamModel courseModuleExamModel) {
        ReturnFormat returnFormat = new ReturnFormat();

        try {
            if (!courseModuleExamModel.equals(null)) {
                returnFormat.setData(courseModuleExamRepo.save(courseModuleExamModel));
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);

            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.DatabaseError);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat addCourseToDegree(CreditModel creditModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            if (!creditModel.equals(null)) {
                returnFormat.setData(creditRepo.save(creditModel));
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);

            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.DatabaseError);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat getCompulsoryStatus() {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<CompulsoryModel> compulsoryModelList = compulsoryRepo.findAll();
            if (compulsoryModelList.size() != 0) {
                returnFormat.setData(compulsoryModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);

            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.DatabaseError);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat setEnrolledCourse(EnrolledCourseViewModel enrolledCourseViewModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            ResultModel resultModel = new ResultModel();
            EnrolledCourseViewModel getCourseModuleId = new EnrolledCourseViewModel();
            getCourseModuleId = enrolledCourseViewRepo.getCourseModuleExamId(enrolledCourseViewModel.getStudent_id(), enrolledCourseViewModel.getDid(), enrolledCourseViewModel.getSmid(), enrolledCourseViewModel.getCid());
            if (!getCourseModuleId.equals(null)) {
                resultModel.setEnrolled(true);
                resultModel.setCourse_module_exam_id(getCourseModuleId.getCmid());
                resultModel.setStudent_id(enrolledCourseViewModel.getStudent_id());
                resultModel.setResult_type_id(1);
                returnFormat.setData(resultRepo.save(resultModel));
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoDataGivenSpecifications);
            }


        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }


    public ReturnFormat removeCourseFromDegree(CreditModel creditModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<CourseModuleExamModel> checkRecordsExits = courseModuleExamRepo.checkCourseModuleExam(creditModel.getDegree_id(), creditModel.getCourse_id());
            if (checkRecordsExits.size() == 0) {
                List<CreditModel> creditModelList = creditRepo.removeCourseFromDegree(creditModel.getDegree_id(), creditModel.getCourse_id());
                if (creditModelList.size() == 0) {
                    returnFormat.setData(null);
                    returnFormat.setMessage(Messages.Deleted);
                    returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                } else {
                    returnFormat.setMessage(Messages.NoRecords);
                    returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
                }
            } else {
                returnFormat.setData(checkRecordsExits);
                returnFormat.setMessage("This Course is Assign to a Semester");
                returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }


    public ReturnFormat removeCourseFromSemester(CourseModuleExamModel courseModuleExamModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            CourseModuleExamModel getCourseModuleId = new CourseModuleExamModel();
            getCourseModuleId = courseModuleExamRepo.getCourseModuleExamId(courseModuleExamModel.getDegree_id(), courseModuleExamModel.getSemester_id(), courseModuleExamModel.getCourse_id());

            if (getCourseModuleId != null) {
                List<ResultModel> resultModelList = resultRepo.checkResultAvailable(getCourseModuleId);
                if (resultModelList.size() == 0) {
                    List<CourseModuleExamModel> courseModuleExamModelList = courseModuleExamRepo.removeCourseFromSemester(courseModuleExamModel.getDegree_id(), courseModuleExamModel.getCourse_id(), courseModuleExamModel.getSemester_id());
                    if (courseModuleExamModelList.size() == 0) {
                        returnFormat.setData(null);
                        returnFormat.setMessage(Messages.Deleted);
                        returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                    } else {
                        returnFormat.setMessage(Messages.NoRecords);
                        returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
                    }
                } else {
                    returnFormat.setData(resultModelList);
                    returnFormat.setMessage(Messages.UnSuccess);
                    returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                }

            } else {
                returnFormat.setData(null);
                returnFormat.setMessage("Course Module Exam Id Not Found");
                returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                return returnFormat;
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat confirmEnrolment(ConfirmEnrollmentModel confirmEnrollmentModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        ConfirmEnrollmentModel enrollmentModelSave = new ConfirmEnrollmentModel();
        try {
            if (confirmEnrollmentModel != null) {
                enrollmentModelSave.setConfirmed(true);
                enrollmentModelSave.setStudent_id(confirmEnrollmentModel.getStudent_id());
                enrollmentModelSave.setSemester_id(confirmEnrollmentModel.getSemester_id());
                confirmEnrollmentRepo.save(enrollmentModelSave);
                returnFormat.setData(enrollmentModelSave);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage("No Data Saved");
                returnFormat.setStatus(ReturnFormat.Status.ERROR);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;

    }

    public ReturnFormat getEnrolmentConfirmation(int studentId, int semesterId) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<ConfirmEnrollmentModel> confirmEnrollmentModelList = confirmEnrollmentRepo.getConfirmedEnrollment(studentId, semesterId);
            if (confirmEnrollmentModelList.size() != 0) {
                returnFormat.setData(confirmEnrollmentModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoDataGivenSpecifications);
            }

        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat unEnrollCourse(EnrolledCourseViewModel enrolledCourseViewModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            CourseModuleExamModel getCourseModuleId = new CourseModuleExamModel();
            getCourseModuleId = courseModuleExamRepo.getCourseModuleExamId(enrolledCourseViewModel.getDid(), enrolledCourseViewModel.getSmid(), enrolledCourseViewModel.getCid());

            if (getCourseModuleId != null) {
                List<ResultModel> resultModelList = resultRepo.checkResultAvailable(getCourseModuleId);
                if (resultModelList.size() != 0) {
                    List<ResultModel> modelList = resultRepo.unEnrollCourse(enrolledCourseViewModel.getStudent_id(), getCourseModuleId.getId());
                    if (modelList.size() == 0) {
                        returnFormat.setData(null);
                        returnFormat.setMessage(Messages.Deleted);
                        returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
                    } else {
                        returnFormat.setMessage(Messages.NoRecords);
                        returnFormat.setStatus(ReturnFormat.Status.NOT_FOUND);
                    }
                } else {
                    returnFormat.setData(resultModelList);
                    returnFormat.setMessage(Messages.UnSuccess);
                    returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                }

            } else {
                returnFormat.setData(null);
                returnFormat.setMessage("Course Module Exam Id Not Found");
                returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
                return returnFormat;
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }

    public ReturnFormat removeEnrolmentConfirmation(RemoveEnrolmentConfirmationModel removeEnrolmentConfirmationModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            if(!removeEnrolmentConfirmationModel.equals(null)){
               removeEnrolmentConfirmationRepo.updateConfirmation(removeEnrolmentConfirmationModel.getStudent_id(),removeEnrolmentConfirmationModel.getSemester_id());
                returnFormat.setMessage(Messages.SuccessUpdate);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            }else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.InternalServerError);
                returnFormat.setStatus(ReturnFormat.Status.UNSUCCESS);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }
}


