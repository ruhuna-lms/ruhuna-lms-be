package com.rusl.lms.RuhunaLms.service;

import com.rusl.lms.RuhunaLms.dao.DepartmentRepo;
import com.rusl.lms.RuhunaLms.model.DepartmentModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import com.rusl.lms.RuhunaLms.utils.Messages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-14
 * Version 1.0
 */

@Service
public class DepartmentService {
    Logger logger = LogManager.getLogger("DepartmentService.class");
    @Autowired
    private DepartmentRepo departmentRepo;

    public ReturnFormat getDepartments(DepartmentModel departmentModel) {
        ReturnFormat returnFormat = new ReturnFormat();
        try {
            List<DepartmentModel> departmentModelList = departmentRepo.findAll();
            if (departmentModelList.size() != 0) {

                returnFormat.setData(departmentModelList);
                returnFormat.setMessage(Messages.Success);
                returnFormat.setStatus(ReturnFormat.Status.SUCCESS);
            } else {
                returnFormat.setData(null);
                returnFormat.setMessage(Messages.NoRecords);
            }
        } catch (JDBCConnectionException e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.ConnectionError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);

        } catch (Exception e) {
            logger.error(e);
            returnFormat.setData(null);
            returnFormat.setMessage(Messages.InternalServerError);
            returnFormat.setStatus(ReturnFormat.Status.ERROR);
        }
        return returnFormat;
    }


}
