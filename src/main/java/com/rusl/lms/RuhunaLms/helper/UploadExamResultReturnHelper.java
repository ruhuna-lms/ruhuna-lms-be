package com.rusl.lms.RuhunaLms.helper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.List;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UploadExamResultReturnHelper {
    private List<String> notRegisteredStudentList;
    private List<String> notEnrolledStudentList;

    public void setNotRegisteredStudentList(List<String> notRegisteredStudentList) {
        this.notRegisteredStudentList = notRegisteredStudentList;
    }

    public void setNotEnrolledStudentList(List<String> notEnrolledStudentList) {
        this.notEnrolledStudentList = notEnrolledStudentList;
    }
}
