package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "resultmodelview")
public class ResultViewModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int rownumber;
    private int sid;
    private int semester_Id;
    private String sname;
    private String CODE;
    private String NAME;
    private double credits;
    private boolean gpa_included;
    private String type;

}
