package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "marksview")
public class GpaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int rownumber;
    private int studentId;
    private double credits;
    private double mark;
}
