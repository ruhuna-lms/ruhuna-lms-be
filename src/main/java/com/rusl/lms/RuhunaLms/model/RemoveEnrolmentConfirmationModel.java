package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "enrolment_confirmation")
public class RemoveEnrolmentConfirmationModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private boolean confirmed;
    private int student_id;
    private int semester_id;
}
