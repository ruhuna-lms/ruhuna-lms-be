package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Date;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-03-16
 * Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "currentSemesterModel")
public class CurrentSemesterModel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int sem_Id;
    private String name;
    private Date start_date;
    private Date end_date;
}
