package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-11
 * Updated by Rusiru on 2021-02-11
 * Updated by Rusiru on 2021-02-14
 * Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String title;
    private String firstname;
    private String lastname;
    private String address;
    private String gender;
    private Date dob;
    private String contact_number;
    private int userrole_id;
    private int department_id;



}
