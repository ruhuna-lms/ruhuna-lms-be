package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "alreadyConnectedCoursesView")
public class AlreadyConnectedCourseModel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String CODE;
    private int degree_id;
    private int semester_id;
    private String name;
    private boolean gpa_included;
    private double credits;



}
