package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Date;
/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "semester")
public class SemesterModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private Date start_date;
    private Date end_date;
}
