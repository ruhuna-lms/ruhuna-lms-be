package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Date;
/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-14
 * Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "StudentView")
public class StudentViewModel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private int userid;
    private String title;
    private String username;
    private String firstname;
    private String lastname;
    private String address;
    private String gender;
    private Date dob;
    private String contact_number;
    private String resgistration_number;
    private int degree_id;
    private int department_id;
    private String index_number;
    private int level;
    private String name_with_initials;
    private String full_name;
    private int specialization_id;
}
