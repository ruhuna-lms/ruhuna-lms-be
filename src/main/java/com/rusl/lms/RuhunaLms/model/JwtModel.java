package com.rusl.lms.RuhunaLms.model;

public class JwtModel {

    private int id;
    private int student_id;
    private String username;
    private int userrole_id;
    private int degree_id;
    private String jwtToken;

    public JwtModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public int getUserrole_id() {
        return userrole_id;
    }

    public void setUserrole_id(int userrole_id) {
        this.userrole_id = userrole_id;
    }

    public int getDegree_id() {
        return degree_id;
    }

    public void setDegree_id(int degree_id) {
        this.degree_id = degree_id;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
