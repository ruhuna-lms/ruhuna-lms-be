package com.rusl.lms.RuhunaLms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-21
 * Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "semesterView")
public class SemesterViewModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int rownumber;
    private int id;
    private String name;


}
