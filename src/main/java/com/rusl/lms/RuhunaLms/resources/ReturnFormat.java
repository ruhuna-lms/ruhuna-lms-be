package com.rusl.lms.RuhunaLms.resources;


/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-11
 * Version 1.0
 */

public class ReturnFormat {
    private Object keys;
    private int pageCount;
    private String type;
    private Object data;
    private Object labels;
    private String message;
    private Status status;

    public ReturnFormat() {
    }

    public ReturnFormat(Object data, String message, Status status) {
        super();
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public ReturnFormat(Object keys, int pageCount, String type, Object data, Object labels, String message, Status status) {
        this.keys = keys;
        this.pageCount = pageCount;
        this.type = type;
        this.data = data;
        this.labels = labels;
        this.message = message;
        this.status = status;
    }

    public ReturnFormat(Object keys, int pageCount, String type, Object data, Object labels, String message) {
        this.keys = keys;
        this.pageCount = pageCount;
        this.type = type;
        this.data = data;
        this.labels = labels;
        this.message = message;
    }

    public ReturnFormat(Status status, String message, Object data, int pageCount) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.pageCount = pageCount;
    }

    public ReturnFormat(Object data, Object labels) {

        this.data = data;
        this.labels = labels;
    }

    public Object getKeys() {
        return keys;
    }

    public void setKeys(Object keys) {
        this.keys = keys;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getLabels() {
        return labels;
    }

    public void setLabels(Object labels) {
        this.labels = labels;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public enum Status {
        SUCCESS, ERROR, CREATED, UPDATED, DELETED, FOUND, NOT_FOUND, UNSUCCESS
    }

}
