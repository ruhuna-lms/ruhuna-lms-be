package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.CurrentSemesterModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CurrentSemesterRepo extends JpaRepository<CurrentSemesterModel, Integer> {
    @Query(value = "{CALL Proc_getcurrentsemester(:degreeId)}", nativeQuery = true)
    List<CurrentSemesterModel> getSemester(@Param("degreeId") int degreeId);
}
