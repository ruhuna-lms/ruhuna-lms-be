package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.LoginModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepo extends JpaRepository<LoginModel,Integer> {
    LoginModel findByusername(@Param("username") String username);
}
