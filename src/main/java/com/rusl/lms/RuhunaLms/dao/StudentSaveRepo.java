package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.StudentModel;
import com.rusl.lms.RuhunaLms.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface StudentSaveRepo extends JpaRepository<StudentModel, Integer> {
    @Query(value = "UPDATE student SET resgistration_number=:resgistration_number,degree_id=:degree_id,index_number=:index_number,level=:level,name_with_initials=:name_with_initials,full_name=:full_name,specialization_id=:specialization_id WHERE user_id=:user_id", nativeQuery = true)
    StudentModel updateStudent(int user_id,String resgistration_number, int degree_id, String index_number, int level, String name_with_initials, String full_name, int specialization_id);


//    @Query(value = "UPDATE user SET title=:title,firstname=:firstname,lastname=:lastname,address=:address,gender=:gender,dob=:dob,contact_number=:contact_number WHERE id=:id", nativeQuery = true)
//    UserModel updateProfile(int id, String title, String firstname, String lastname, String address, String gender, Date dob, String contact_number);

}
