package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.DegreeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */

@Repository
public interface DegreeRepo extends JpaRepository<DegreeModel, Integer> {
//    DegreeModel findByName(String name);
}
