package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.CreditModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
@Repository
public interface CreditRepo extends JpaRepository<CreditModel,Integer> {
    @Query(value = "DELETE FROM credit WHERE degree_id=:degreeId AND course_id=:courseId", nativeQuery = true)
    List<CreditModel> removeCourseFromDegree(@Param("degreeId") int degreeId, @Param("courseId") int courseId);

}
