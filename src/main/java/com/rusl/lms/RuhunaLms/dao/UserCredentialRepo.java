package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.UserCredentialModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
@Repository
public interface UserCredentialRepo extends JpaRepository<UserCredentialModel,Integer> {
    @Query(value = "SELECT id,user_id,password FROM usercredentials WHERE user_id=:id", nativeQuery = true)
    Optional<UserCredentialModel> getOldPassword(int id);
    @Query(value = "UPDATE usercredentials SET PASSWORD=:encryptedNewPassword WHERE user_id=:user_id", nativeQuery = true)
    UserCredentialModel updatePassword(int user_id, String encryptedNewPassword);

}
