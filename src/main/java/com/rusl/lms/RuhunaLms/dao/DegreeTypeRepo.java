package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.DegreeTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
@Repository
public interface DegreeTypeRepo extends JpaRepository<DegreeTypeModel,Integer> {
}
