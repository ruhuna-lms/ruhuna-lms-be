package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.ResultViewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-21
 * Updated by Rusiru on 2022-02-20
 * Version 1.0
 */

@Repository
public interface ResultViewRepo extends JpaRepository<ResultViewModel, Integer> {
    @Query(value = "{CALL Proc_resultmodelview(:studentId, :semesterId)}", nativeQuery = true)
    List<ResultViewModel> getResults(@Param("studentId") int studentId, @Param("semesterId") int semesterId);

    @Query(value = "{CALL Proc_allresultmodelview(:studentId)}", nativeQuery = true)
    List<ResultViewModel> getAllSemesterResults(@Param("studentId") int studentId);
}
