package com.rusl.lms.RuhunaLms.dao;


import com.rusl.lms.RuhunaLms.model.StudentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
@Repository
public interface StudentRepo extends JpaRepository<StudentModel, Integer> {
    @Query(value = "SELECT id,resgistration_number,degree_id,user_id,index_number,`level`,name_with_initials,full_name,specialization_id FROM student WHERE user_id=:userId", nativeQuery = true)
    StudentModel getStudentByUserId(@Param("userId") int userId);

    @Query(value = "SELECT id,resgistration_number,degree_id,user_id,index_number,`level`,name_with_initials,full_name,specialization_id FROM student WHERE resgistration_number=:registrationNumber", nativeQuery = true)
    StudentModel getStudentByRegistrationNumber(@Param("registrationNumber") String registrationNumber);
}
