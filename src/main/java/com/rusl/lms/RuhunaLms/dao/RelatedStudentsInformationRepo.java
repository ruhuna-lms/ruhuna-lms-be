package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.CourseModuleExamModel;
import com.rusl.lms.RuhunaLms.model.RelatedStudentInformationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2022-02-20
 * Version 1.0
 */

@Repository
public interface RelatedStudentsInformationRepo extends JpaRepository<RelatedStudentInformationModel,Integer> {
    @Query(value = "SELECT r.student_id,s.resgistration_number,u.title,u.firstname,u.lastname,course_module_exam_id FROM result r,student s, user u WHERE r.course_module_exam_id=:getCourseModuleId AND r.student_id=s.id AND u.id=s.user_id", nativeQuery = true)
    List<RelatedStudentInformationModel> getRelatedStudentsList(CourseModuleExamModel getCourseModuleId);
}
