package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.CompulsoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
public interface CompulsoryRepo extends JpaRepository<CompulsoryModel,Integer> {

}
