package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.DepartmentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-14
 * Version 1.0
 */

@Repository
public interface DepartmentRepo extends JpaRepository<DepartmentModel,Integer> {
}
