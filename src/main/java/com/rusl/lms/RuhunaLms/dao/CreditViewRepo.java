package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.CreditViewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CreditViewRepo extends JpaRepository<CreditViewModel,String> {
    @Query(value = "SELECT id,CODE,NAME,gpa_included,compulsory,credits FROM creditview WHERE degree_id=:degreeId",nativeQuery = true)
    List<CreditViewModel> getAllCoursesByDegree(@Param("degreeId") int degreeId);
}
