package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.StudentViewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-13
 * Version 1.0
 */
public interface StudentViewRepo extends JpaRepository<StudentViewModel,Integer> {
    @Query(value = "SELECT id,userid,title,firstname,lastname,address,contact_number,username,dob,gender,resgistration_number,degree_id,department_id,index_number,`level`,name_with_initials,full_name,specialization_id  FROM student_view",nativeQuery = true)
    List<StudentViewModel> getStudents();
}
