package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.EnrolledCoursesModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnrollCoursesRepo extends JpaRepository<EnrolledCoursesModel, Integer> {
    @Query(value = "SELECT id,CODE,NAME,gpa_included,TYPE,credits FROM allenrolledcoursesview e WHERE e.sid=:sid", nativeQuery = true)
    List<EnrolledCoursesModel> getAllEnrollCourses(@Param("sid") int sid);


}
