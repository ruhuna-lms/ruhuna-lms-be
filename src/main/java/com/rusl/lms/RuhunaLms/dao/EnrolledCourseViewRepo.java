package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.EnrolledCourseViewModel;
import com.rusl.lms.RuhunaLms.model.EnrolledCoursesModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
@Repository
public interface EnrolledCourseViewRepo extends JpaRepository<EnrolledCourseViewModel,Integer> {
    @Query(value = "SELECT student_id,did,smid,cid,cmid FROM enrolledcourseview WHERE student_id=:student_id AND did=:did AND smid=:smid AND cid=:cid", nativeQuery = true)
    EnrolledCourseViewModel getCourseModuleExamId(@Param("student_id") int student_id, @Param("did") int did, @Param("smid") int smid, @Param("cid") int cid);
}
