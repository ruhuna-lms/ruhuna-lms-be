package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.ResultModel;
import com.rusl.lms.RuhunaLms.model.UserModel;
import com.rusl.lms.RuhunaLms.model.UserStudentModel;
import com.rusl.lms.RuhunaLms.resources.ReturnFormat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface UserRepo extends JpaRepository<UserModel,Integer> {
    @Query(value = "SELECT id,username,title,firstname,lastname,address,gender,dob,contact_number,userrole_id,department_id  FROM user WHERE username=:getUserName", nativeQuery = true)
    UserModel getUserName(String getUserName);

    @Query(value = "UPDATE user u SET u.title=:title,u.firstname=:firstname,u.lastname=:lastname,u.address=:address,u.gender=:gender,u.dob=:dob,u.contact_number=:contact_number WHERE u.id=:userid", nativeQuery = true)
    UserModel updateProfile(int userid, String title,String firstname, String lastname, String address, String gender, Date dob, String contact_number);

}
