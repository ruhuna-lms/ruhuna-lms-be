package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.SemesterViewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-21
 * Version 1.0
 */
@Repository
public interface StudentsSemestersRepo extends JpaRepository<SemesterViewModel, Integer> {
    @Query(value = "{CALL Proc_semesterview(:studentId)}", nativeQuery = true)
    List<SemesterViewModel>getSemesterForStudent(@Param("studentId") int studentId);
}
