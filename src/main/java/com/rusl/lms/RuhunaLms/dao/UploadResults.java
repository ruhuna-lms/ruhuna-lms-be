package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.UploadResultsModel;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-03-23
 * Version 1.0
 */
public interface UploadResults extends JpaRepository<UploadResultsModel,Integer> {
}
