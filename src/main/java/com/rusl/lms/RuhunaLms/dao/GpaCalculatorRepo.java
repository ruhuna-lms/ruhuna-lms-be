package com.rusl.lms.RuhunaLms.dao;


import com.rusl.lms.RuhunaLms.model.GpaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GpaCalculatorRepo extends JpaRepository<GpaModel, Integer> {
    @Query(value = "{CALL Proc_marksview(:studentId)}", nativeQuery = true)
    List<GpaModel> getMarks(@Param("studentId") int studentId);
}
