package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.CourseModuleExamModel;
import com.rusl.lms.RuhunaLms.model.CreditModel;
import com.rusl.lms.RuhunaLms.model.EnrolledCourseViewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseModuleExamRepo extends JpaRepository<CourseModuleExamModel, Integer> {
    @Query(value = "SELECT cme.id,cme.course_id,cme.semester_id,cme.degree_id FROM course_module_exam cme WHERE cme.degree_id=:degreeId AND cme.semester_id=:semesterId AND cme.course_id=:courseId", nativeQuery = true)
    CourseModuleExamModel getCourseModuleExamId(@Param("degreeId") int degreeId, @Param("semesterId") int semesterId, @Param("courseId") int courseId);

    @Query(value = "DELETE FROM course_module_exam  WHERE degree_id=:degreeId AND course_id=:courseId AND semester_id=:semesterId", nativeQuery = true)
    List<CourseModuleExamModel> removeCourseFromSemester(@Param("degreeId") int degreeId, @Param("courseId") int courseId, @Param("semesterId") int semesterId);

    @Query(value = "SELECT cme.id,cme.course_id,cme.semester_id,cme.degree_id FROM course_module_exam cme WHERE cme.degree_id=:degreeId AND cme.course_id=:courseId", nativeQuery = true)
    List<CourseModuleExamModel> checkCourseModuleExam(@Param("degreeId") int degreeId, @Param("courseId") int courseId);
}
