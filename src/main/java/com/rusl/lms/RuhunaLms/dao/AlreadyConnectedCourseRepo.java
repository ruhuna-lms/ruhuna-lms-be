package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.AlreadyConnectedCourseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-19
 * Version 1.0
 */

@Repository
public interface AlreadyConnectedCourseRepo extends JpaRepository<AlreadyConnectedCourseModel,Integer> {
    @Query(value = "SELECT acc.id,acc.CODE,acc.degree_id,acc.semester_id,acc.name,acc.gpa_included,acc.credits FROM alreadyconnectedcoursesview acc WHERE acc.degree_id=:degreeId AND acc.semester_id=:semesterId",nativeQuery = true)
    List<AlreadyConnectedCourseModel> getAlreadyConnectedCourses(@Param("degreeId") int degreeId, @Param("semesterId") int semesterId);
}
