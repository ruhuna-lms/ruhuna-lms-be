package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.ConfirmEnrollmentModel;
import com.rusl.lms.RuhunaLms.model.EnrolledCoursesModel;
import com.rusl.lms.RuhunaLms.model.StudentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfirmEnrollmentRepo extends JpaRepository<ConfirmEnrollmentModel, Integer> {

    @Query(value = "SELECT ec.id,ec.confirmed,ec.student_id,ec.semester_id FROM enrolment_confirmation ec WHERE ec.student_id=:studentId AND ec.semester_id=:semesterId", nativeQuery = true)
    List<ConfirmEnrollmentModel> getConfirmedEnrollment(@Param("studentId") int studentId, @Param("semesterId") int semesterId);

//    @Query(value = "UPDATE enrolment_confirmation SET confirmed=0 WHERE student_id=:student_id and semester_id=:semester_id;", nativeQuery = true)
//    ConfirmEnrollmentModel updateConfirmation(int student_id,int semester_id);

}
