package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.ConfirmEnrollmentModel;
import com.rusl.lms.RuhunaLms.model.RemoveEnrolmentConfirmationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RemoveEnrolmentConfirmationRepo extends JpaRepository<RemoveEnrolmentConfirmationModel ,Integer > {
    @Query(value = "DELETE FROM enrolment_confirmation  WHERE student_id=:student_id and semester_id=:semester_id", nativeQuery = true)
    ConfirmEnrollmentModel updateConfirmation(int student_id, int semester_id);

}
