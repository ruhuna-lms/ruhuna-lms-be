package com.rusl.lms.RuhunaLms.dao;

import com.rusl.lms.RuhunaLms.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-22
 * Version 1.0
 */
@Repository
public interface ResultRepo extends JpaRepository<ResultModel, Integer> {
    @Query(value = "SELECT r.id,r.enrolled,r.student_id,r.course_module_exam_id,r.result_type_id FROM result r WHERE r.student_id=:studentId AND r.course_module_exam_id=:courseModuleExamId", nativeQuery = true)
    ResultModel getResult(@Param("studentId") int studentId, @Param("courseModuleExamId") int courseModuleExamId);

    @Query(value = "UPDATE result r SET r.result_type_id=:resultTypeId WHERE r.id=:resultId", nativeQuery = true)
    ResultModel updateResult(int resultTypeId, int resultId);

    @Query(value = "SELECT r.id,r.enrolled,r.student_id,r.course_module_exam_id,r.result_type_id FROM result r WHERE r.course_module_exam_id=:getCourseModuleId", nativeQuery = true)
    List<ResultModel> checkResultAvailable(CourseModuleExamModel getCourseModuleId);

   @Query(value = "DELETE FROM result  WHERE student_id=:studentId AND course_module_exam_id=:courseModuleExamId", nativeQuery = true)
    List<ResultModel> unEnrollCourse(@Param("studentId") int studentId, @Param("courseModuleExamId") int courseModuleExamId);



}
