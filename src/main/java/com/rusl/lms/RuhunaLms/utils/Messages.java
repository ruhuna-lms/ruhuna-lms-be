package com.rusl.lms.RuhunaLms.utils;


import java.util.ArrayList;
import java.util.List;

/**
 * Project University of Ruhuna Learning Management System
 * Created by Rusiru on 2021-02-11
 * Updated by Rusiru on 2021-02-13
 * Version 1.0
 */

public class Messages {
    public static final String Success = "ok";
    public static final String EmptyArray = "Empty array has been generated";
    public static final String ConnectionError = "Please check the connection";
    public static final String IncorrectDateRange = "Cannot access the given date range";
    public static final String NoRecords = "No Records Found";
    public static final String SuccessUpdate = "Successfully Updated";
    public static final String DatabaseError = "Database Error Occurred";
    public static final String InternalServerError = "Something went wrong";
    public static final String NoDataGivenRange = "No data available for the given date range";
    public static final String ArithmeticError = "Error occurred in arithmetic operation";
    public static final String NoDataGivenSpecifications = "No data available for the given specifications";
    public static final String AuthenticationError = "Unauthorized User Error";
    public static final String NoDataAvailable = "No data available";
    public static final String Deleted = "Data Deleted Success";
    public static final String UnSuccess = "Cannot Delete This Record";
    public static final String INVALID_LOGIN_CREDENTIALS = "Invalid Credentials";

    public Messages() {
    }

    public List<String> getMessages() {
        List<String> constants = new ArrayList<String>();
        constants.add(Success);
        constants.add(EmptyArray);
        constants.add(ConnectionError);
        constants.add(IncorrectDateRange);
        constants.add(NoRecords);
        constants.add(SuccessUpdate);
        constants.add(DatabaseError);
        constants.add(InternalServerError);
        constants.add(NoDataGivenRange);
        constants.add(ArithmeticError);
        constants.add(NoDataGivenSpecifications);
        constants.add(AuthenticationError);
        constants.add(NoDataAvailable);
        constants.add(INVALID_LOGIN_CREDENTIALS);
        constants.add(Deleted);
        constants.add(UnSuccess);
        return constants;
    }
}
